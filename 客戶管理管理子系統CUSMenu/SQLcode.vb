﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Data
Imports System
Imports System.Windows.Forms
Imports System.Data.Odbc



Public Class SQLcode
    Public Shared Sub SetData(ByVal strSQL As String)
        Dim strConn As String = System.Configuration.ConfigurationManager.ConnectionStrings("客戶管理管理子系統CUSMenu.My.MySettings.customerConnectionString").ToString()

        Dim objConn As New Odbc.OdbcConnection(strConn)
        Dim objCmd As New Odbc.OdbcCommand(strSQL, objConn)

        objConn.Open()
        objCmd.ExecuteNonQuery()
        objConn.Close()
        objConn.Dispose()
    End Sub
    Public Shared Function GetData(ByVal strSQL As String) As DataTable
        Dim strConn As String = System.Configuration.ConfigurationManager.ConnectionStrings("客戶管理管理子系統CUSMenu.My.MySettings.customerConnectionString").ToString()

        Dim objConn As New Odbc.OdbcConnection(strConn)
        Dim objADP As New Odbc.OdbcDataAdapter(strSQL, objConn)
        Dim dt As DataTable = New DataTable
        objConn.Open()
        objADP.Fill(dt)
        Return dt
        objConn.Close()
        objConn.Dispose()
    End Function
End Class


