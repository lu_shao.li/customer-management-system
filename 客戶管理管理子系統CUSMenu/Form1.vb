﻿Public Class Form1

    Dim type As String = ""
    Dim Max As Integer = 0
    Private Sub buttonInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonInsert.Click
        type = "新增"
        labelState.Text = "作業訊息：目前為﹝新增﹞作業模式"
        btnClose()                  '關閉全部按鈕
        TextOpen()                  '打開Text
        button4.Enabled = True      '打開確認和取消
        button5.Enabled = True
        dataGridView1.Enabled = False
        If dataGridView1.SelectedRows.Count > 0 Then
            dataGridView1.SelectedRows(0).Selected = False
        End If

        TextClr()
        button7.Enabled = False
        button8.Enabled = False
        button9.Enabled = False
        button10.Enabled = False
    End Sub

    Private Sub FExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FExit.Click
        '離開
        TextClose()
        panel2.Visible = False
        dt.Rows.Clear()
        labelState.Text = ""
    End Sub

    Private Sub 客戶基本資料ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 客戶基本資料ToolStripMenuItem.Click
        panel2.Visible = True
        ChkDGVCount()
        TextClose()
        TextClr()
        ChkSelectIndex()
    End Sub
    Public dt As DataTable = New DataTable
    Private Sub button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button1.Click
        '查詢
        'If textBoxSearch.TextLength = 0 Then
        '    MsgBox("請輸入電話!!")
        'ElseIf textBoxSearch.TextLength < 4 Then
        '    MsgBox("電話請輸入超過四碼!!")
        'Else
        dataGridView1.Columns.Clear()
        dt = SQLcode.GetData("select * from customer where TEL1 like '%" & textBoxSearch.Text & "%' or TEL2 like '%" & textBoxSearch.Text & "%'")
        If dt.Rows.Count > 0 Then
            dataGridView1.DataSource = dt
            For n = 0 To dataGridView1.Columns.Count - 1
                dataGridView1.Columns(n).SortMode = False
            Next
            dataGridView1.Rows(0).Selected = False
            ChkData()
        Else
            MsgBox("查無資料!!")
            dt.Rows.Clear()
            dataGridView1.DataSource = dt
        End If

        ChkDGVCount()
        TextClose()
        TextClr()

        dataGridView1.Columns(0).HeaderText = "姓名"
        dataGridView1.Columns(1).HeaderText = "介紹人"
        dataGridView1.Columns(2).HeaderText = "電話一"
        dataGridView1.Columns(3).HeaderText = "電話二"
        dataGridView1.Columns(4).HeaderText = "電話三"
        dataGridView1.Columns(5).HeaderText = "電話四"
        dataGridView1.Columns(6).HeaderText = "職業"
        dataGridView1.Columns(7).Visible = False
        ChkSelectIndex()


        'End If
    End Sub

    Private Sub button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button4.Click
        '確定
        If type = "新增" Then
            Dim str() As String = {textBoxCName.Text, textBoxRName.Text, textBoxTEL1.Text, textBoxTEL2.Text, textBoxTEL3.Text, textBoxTEL4.Text, textBoxOccupation.Text, textBoxRemark.Text}
            dt.Rows.Add(str)
            SQLcode.SetData("insert into customer values('" & textBoxCName.Text & "','" & textBoxRName.Text & "','" & textBoxTEL1.Text & "','" & textBoxTEL2.Text & "','" & textBoxTEL3.Text & "','" & textBoxTEL4.Text & "','" & textBoxOccupation.Text & "','" & textBoxRemark.Text & "')")
            dataGridView1.Rows(dataGridView1.Rows.Count - 1).Selected = True
            ChkData()
        ElseIf type = "修改" Then
            Dim sql As String = "update customer set NAME_C='" & textBoxCName.Text & "',NAME_INST='" & textBoxRName.Text & "',TEL1='" & textBoxTEL1.Text & "',TEL2='" & textBoxTEL2.Text & "',S1='" & textBoxTEL3.Text & "',S2='" & textBoxTEL4.Text & "',REMARK='" & textBoxOccupation.Text & "',DETAIL='" & textBoxRemark.Text & "' where "
            If Not dataGridView1.SelectedRows(0).Cells(0).Value Is DBNull.Value Then
                sql &= "NAME_C='" & dataGridView1.SelectedRows(0).Cells(0).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(1) Is DBNull.Value Then
                sql &= " and NAME_INST='" & dataGridView1.SelectedRows(0).Cells(1).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(2) Is DBNull.Value Then
                sql &= " and TEL1='" & dataGridView1.SelectedRows(0).Cells(2).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(3) Is DBNull.Value Then
                sql &= " and TEL2='" & dataGridView1.SelectedRows(0).Cells(3).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(4) Is DBNull.Value Then
                sql &= " and S1='" & dataGridView1.SelectedRows(0).Cells(4).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(5) Is DBNull.Value Then
                sql &= " and S2='" & dataGridView1.SelectedRows(0).Cells(5).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(6) Is DBNull.Value Then
                sql &= " and REMARK='" & dataGridView1.SelectedRows(0).Cells(6).Value & "'"
            End If
            If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(7) Is DBNull.Value Then
                sql &= " and DETAIL='" & dataGridView1.SelectedRows(0).Cells(7).Value & "'"
            End If
            SQLcode.SetData(sql)
            dataGridView1.SelectedRows(0).Cells(0).Value = textBoxCName.Text
            dataGridView1.SelectedRows(0).Cells(1).Value = textBoxRName.Text
            dataGridView1.SelectedRows(0).Cells(2).Value = textBoxTEL1.Text
            dataGridView1.SelectedRows(0).Cells(3).Value = textBoxTEL2.Text
            dataGridView1.SelectedRows(0).Cells(4).Value = textBoxTEL3.Text
            dataGridView1.SelectedRows(0).Cells(5).Value = textBoxTEL4.Text
            dataGridView1.SelectedRows(0).Cells(6).Value = textBoxOccupation.Text
            dataGridView1.SelectedRows(0).Cells(7).Value = textBoxRemark.Text
        End If
        labelState.Text = ""
        TextClose()
        ChkDGVCount()
        ChkSelectIndex()
    End Sub

    Private Sub button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button5.Click
        '取消
        If MessageBox.Show("是否取消畫面上編輯資料?", "CUS", MessageBoxButtons.OKCancel) = Windows.Forms.DialogResult.OK Then
            TextClose()
            TextClr()
            dataGridView1.Enabled = True
            FirstBTN()
            TextClose()
            labelState.Text = ""
        End If
    End Sub

    Sub TextClr()
        For Each n In groupBox1.Controls
            If n.GetType Is GetType(TextBox) Then
                CType(n, TextBox).Text = ""
            End If
        Next
    End Sub

    Sub TextOpen()
        For Each i As Object In groupBox1.Controls
            i.Enabled = True
        Next
    End Sub
    Sub TextClose()
        For Each i As Object In groupBox1.Controls
            i.enabled = False
        Next
    End Sub

    Sub btnOpen()
        For Each i As Object In Panel1.Controls
            i.Enabled = True
        Next
    End Sub
    Sub btnClose()
        For Each i As Object In Panel1.Controls
            i.enabled = False
        Next
    End Sub

    Private Sub dataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dataGridView1.CellClick

    End Sub

    Sub FirstBTN()
        btnOpen()
        button4.Enabled = False
        button5.Enabled = False
    End Sub

    Private Sub button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button2.Click
        If dataGridView1.SelectedRows.Count > 0 Then
            type = "修改"
            dataGridView1.Enabled = False
            labelState.Text = "作業訊息：目前為﹝修改﹞作業模式"
            btnClose()                  '關閉全部按鈕
            TextOpen()                  '打開Text
            button4.Enabled = True      '打開確認和取消
            button5.Enabled = True
            dataGridView1.Enabled = False
            button7.Enabled = False
            button8.Enabled = False
            button9.Enabled = False
            button10.Enabled = False
        Else
            MsgBox("請先選取一筆資料")
        End If

    End Sub

    Private Sub dataGridView1_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dataGridView1.SelectionChanged, dataGridView1.CellContentClick
        DGVSelect()

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        panel2.Visible = False
        dataGridView1.MultiSelect = False
        dt = New DataTable()
        dt.Columns.Add("姓名")
        dt.Columns.Add("介紹人")
        dt.Columns.Add("電話一")
        dt.Columns.Add("電話二")
        dt.Columns.Add("電話三")
        dt.Columns.Add("電話四")
        dt.Columns.Add("職業")
        dt.Columns.Add("詳細內容")
        dataGridView1.DataSource = dt
        Form2.Visible = False


    End Sub

    Private Sub button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button3.Click
        If dataGridView1.SelectedRows.Count > 0 Then
            If MessageBox.Show("是否刪除該筆資料", "刪除作業", MessageBoxButtons.OKCancel) = Windows.Forms.DialogResult.OK Then
                Dim sql As String = "delete from customer where "
                If Not dataGridView1.SelectedRows(0).Cells(0).Value Is DBNull.Value Then
                    sql &= "NAME_C='" & dataGridView1.SelectedRows(0).Cells(0).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(1) Is DBNull.Value Then
                    sql &= " and NAME_INST='" & dataGridView1.SelectedRows(0).Cells(1).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(2) Is DBNull.Value Then
                    sql &= " and TEL1='" & dataGridView1.SelectedRows(0).Cells(2).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(3) Is DBNull.Value Then
                    sql &= " and TEL2='" & dataGridView1.SelectedRows(0).Cells(3).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(4) Is DBNull.Value Then
                    sql &= " and S1='" & dataGridView1.SelectedRows(0).Cells(4).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(5) Is DBNull.Value Then
                    sql &= " and S2='" & dataGridView1.SelectedRows(0).Cells(5).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(6) Is DBNull.Value Then
                    sql &= " and REMARK='" & dataGridView1.SelectedRows(0).Cells(6).Value & "'"
                End If
                If Not dt.Rows(dataGridView1.SelectedRows(0).Index).Item(7) Is DBNull.Value Then
                    sql &= " and DETAIL='" & dataGridView1.SelectedRows(0).Cells(7).Value & "'"
                End If
                SQLcode.SetData(sql)
                Dim setNum As Integer = dataGridView1.SelectedRows(0).Index
                dt.Rows.RemoveAt(setNum)

                If dataGridView1.SelectedRows.Count > 0 Then
                    If setNum > 0 Then
                        dataGridView1.SelectedRows(setNum - 1).Selected = True
                    Else
                        dataGridView1.SelectedRows(0).Selected = True
                    End If
                End If
                ChkData()
                ChkDGVCount()
                TextClr()
                ChkSelectIndex()
            End If
        Else
            MsgBox("請先選取資料!!")
        End If

    End Sub

    Sub ChkDGVCount()
        If dataGridView1.Rows.Count > 0 Then
            FirstBTN()
            dataGridView1.Enabled = True
        Else
            ChkSelectIndex()
            FirstBTN()
            button2.Enabled = False
            button3.Enabled = False
        End If
    End Sub

    Sub DGVSelect()
        If dataGridView1.SelectedRows.Count > 0 Then
            If Not IsDBNull(dataGridView1.SelectedRows(0).Cells(0)) Then
                textBoxCName.Text = dataGridView1.SelectedRows(0).Cells(0).Value
            Else
                textBoxCName.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(1).Value Is DBNull.Value Then
                textBoxRName.Text = dataGridView1.SelectedRows(0).Cells(1).Value
            Else
                textBoxRName.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(2).Value Is DBNull.Value Then
                textBoxTEL1.Text = dataGridView1.SelectedRows(0).Cells(2).Value
            Else
                textBoxTEL1.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(3).Value Is DBNull.Value Then
                textBoxTEL2.Text = dataGridView1.SelectedRows(0).Cells(3).Value
            Else
                textBoxTEL2.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(4).Value Is DBNull.Value Then
                textBoxTEL3.Text = dataGridView1.SelectedRows(0).Cells(4).Value
            Else
                textBoxTEL3.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(5).Value Is DBNull.Value Then
                textBoxTEL4.Text = dataGridView1.SelectedRows(0).Cells(5).Value
            Else
                textBoxTEL4.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(6).Value Is DBNull.Value Then
                textBoxOccupation.Text = dataGridView1.SelectedRows(0).Cells(6).Value
            Else
                textBoxOccupation.Text = ""
            End If
            If Not dataGridView1.SelectedRows(0).Cells(7).Value Is DBNull.Value Then
                textBoxRemark.Text = dataGridView1.SelectedRows(0).Cells(7).Value
            Else
                textBoxRemark.Text = ""
            End If
        End If
        ChkSelectIndex()
    End Sub

    Sub ChkSelectIndex()
        ChkData()

        If dataGridView1.Rows.Count > 0 Then
            button7.Enabled = True
            button8.Enabled = False
            button9.Enabled = False
            button10.Enabled = True
            If dataGridView1.SelectedRows.Count > 0 Then
                If dataGridView1.Rows.Count > 1 Then
                    If dataGridView1.SelectedRows(0).Index = 0 Then
                        button7.Enabled = False
                        button8.Enabled = False
                        button9.Enabled = True
                        button10.Enabled = True
                    ElseIf dataGridView1.SelectedRows(0).Index = dataGridView1.Rows.Count - 1 Then
                        button7.Enabled = True
                        button8.Enabled = True
                        button9.Enabled = False
                        button10.Enabled = False
                    Else
                        button7.Enabled = True
                        button8.Enabled = True
                        button9.Enabled = True
                        button10.Enabled = True
                    End If
                Else
                    button7.Enabled = False
                    button8.Enabled = False
                    button9.Enabled = False
                    button10.Enabled = False
                End If
            Else
                button8.Enabled = False
                button9.Enabled = False
            End If
        Else
            button7.Enabled = False
            button8.Enabled = False
            button9.Enabled = False
            button10.Enabled = False
        End If

    End Sub

    Private Sub button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button7.Click
        dataGridView1.Rows(0).Selected = True
    End Sub

    Private Sub button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button10.Click
        dataGridView1.Rows(dataGridView1.Rows.Count - 1).Selected = True
    End Sub

    Private Sub button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button8.Click
        dataGridView1.Rows(dataGridView1.SelectedRows(0).Index - 1).Selected = True
    End Sub

    Private Sub button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button9.Click
        dataGridView1.Rows(dataGridView1.SelectedRows(0).Index + 1).Selected = True
    End Sub

    Sub ChkData()
        Max = dt.Rows.Count
        If dataGridView1.Rows.Count > 0 Then
            If dataGridView1.SelectedRows.Count > 0 Then
                label4.Text = dataGridView1.SelectedRows(0).Index + 1 & " / " & Max
            Else
                label4.Text = 0 & " / " & Max
            End If
        Else
            label4.Text = 0
        End If

    End Sub

    Private Sub 密碼變更ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 密碼變更ToolStripMenuItem.Click
        Form3.Show()
    End Sub

    Private Sub menuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles menuStrip1.ItemClicked

    End Sub

    Private Sub 結束作業系統ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 結束作業系統ToolStripMenuItem.Click

        Dim result As DialogResult
        result = MessageBox.Show("離開客戶子系統", "作業結束", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk)
        If result = DialogResult.Yes Then
            Application.Exit()
            Me.Close()
            Me.Dispose()
        End If

    End Sub
    Private Sub form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.Enabled = False


          If MessageBox.Show("確定關閉視窗?", "警告", MessageBoxButtons.YesNo) = MsgBoxResult.No Then

            e.Cancel = True
            Application.Exit()
            Me.Close()
            Me.Dispose()
        End If

    End Sub


    Private Sub textBoxOccupation_TextChanged(sender As Object, e As EventArgs) Handles textBoxOccupation.TextChanged

    End Sub

    Private Sub dataGridView1_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs)

    End Sub
End Class
