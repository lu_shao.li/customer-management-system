﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.menuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.結束作業系統ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.客戶基本資料ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.密碼變更ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.labelState = New System.Windows.Forms.Label()
        Me.panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.button10 = New System.Windows.Forms.Button()
        Me.FExit = New System.Windows.Forms.Button()
        Me.button9 = New System.Windows.Forms.Button()
        Me.button3 = New System.Windows.Forms.Button()
        Me.button8 = New System.Windows.Forms.Button()
        Me.button2 = New System.Windows.Forms.Button()
        Me.button7 = New System.Windows.Forms.Button()
        Me.button4 = New System.Windows.Forms.Button()
        Me.button5 = New System.Windows.Forms.Button()
        Me.buttonInsert = New System.Windows.Forms.Button()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.textBoxTEL4 = New System.Windows.Forms.TextBox()
        Me.textBoxTEL2 = New System.Windows.Forms.TextBox()
        Me.textBoxRName = New System.Windows.Forms.TextBox()
        Me.label12 = New System.Windows.Forms.Label()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.textBoxRemark = New System.Windows.Forms.TextBox()
        Me.textBoxOccupation = New System.Windows.Forms.TextBox()
        Me.textBoxTEL3 = New System.Windows.Forms.TextBox()
        Me.textBoxTEL1 = New System.Windows.Forms.TextBox()
        Me.textBoxCName = New System.Windows.Forms.TextBox()
        Me.label9 = New System.Windows.Forms.Label()
        Me.label8 = New System.Windows.Forms.Label()
        Me.label7 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.dataGridView1 = New System.Windows.Forms.DataGridView()
        Me.panel3 = New System.Windows.Forms.Panel()
        Me.panel4 = New System.Windows.Forms.Panel()
        Me.textBoxSearch = New System.Windows.Forms.TextBox()
        Me.button1 = New System.Windows.Forms.Button()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label13 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.menuStrip1.SuspendLayout()
        Me.panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel3.SuspendLayout()
        Me.panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'menuStrip1
        '
        Me.menuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.結束作業系統ToolStripMenuItem, Me.客戶基本資料ToolStripMenuItem, Me.密碼變更ToolStripMenuItem})
        Me.menuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip1.Name = "menuStrip1"
        Me.menuStrip1.Size = New System.Drawing.Size(957, 24)
        Me.menuStrip1.TabIndex = 6
        Me.menuStrip1.Text = "menuStrip1"
        '
        '結束作業系統ToolStripMenuItem
        '
        Me.結束作業系統ToolStripMenuItem.Name = "結束作業系統ToolStripMenuItem"
        Me.結束作業系統ToolStripMenuItem.Size = New System.Drawing.Size(91, 20)
        Me.結束作業系統ToolStripMenuItem.Text = "結束作業系統"
        '
        '客戶基本資料ToolStripMenuItem
        '
        Me.客戶基本資料ToolStripMenuItem.Name = "客戶基本資料ToolStripMenuItem"
        Me.客戶基本資料ToolStripMenuItem.Size = New System.Drawing.Size(91, 20)
        Me.客戶基本資料ToolStripMenuItem.Text = "客戶基本資料"
        '
        '密碼變更ToolStripMenuItem
        '
        Me.密碼變更ToolStripMenuItem.Name = "密碼變更ToolStripMenuItem"
        Me.密碼變更ToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.密碼變更ToolStripMenuItem.Text = "密碼變更"
        '
        'labelState
        '
        Me.labelState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.labelState.Font = New System.Drawing.Font("新細明體", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.labelState.Location = New System.Drawing.Point(390, 34)
        Me.labelState.Name = "labelState"
        Me.labelState.Size = New System.Drawing.Size(341, 25)
        Me.labelState.TabIndex = 8
        Me.labelState.Text = "作業訊息:"
        '
        'panel2
        '
        Me.panel2.BackColor = System.Drawing.Color.Silver
        Me.panel2.Controls.Add(Me.Panel1)
        Me.panel2.Controls.Add(Me.groupBox1)
        Me.panel2.Controls.Add(Me.dataGridView1)
        Me.panel2.Controls.Add(Me.panel3)
        Me.panel2.Location = New System.Drawing.Point(12, 21)
        Me.panel2.Name = "panel2"
        Me.panel2.Size = New System.Drawing.Size(801, 574)
        Me.panel2.TabIndex = 9
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.button10)
        Me.Panel1.Controls.Add(Me.FExit)
        Me.Panel1.Controls.Add(Me.button9)
        Me.Panel1.Controls.Add(Me.button3)
        Me.Panel1.Controls.Add(Me.button8)
        Me.Panel1.Controls.Add(Me.button2)
        Me.Panel1.Controls.Add(Me.button7)
        Me.Panel1.Controls.Add(Me.button4)
        Me.Panel1.Controls.Add(Me.button5)
        Me.Panel1.Controls.Add(Me.buttonInsert)
        Me.Panel1.Location = New System.Drawing.Point(266, 502)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(501, 69)
        Me.Panel1.TabIndex = 33
        '
        'button10
        '
        Me.button10.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button10.Location = New System.Drawing.Point(337, 34)
        Me.button10.Name = "button10"
        Me.button10.Size = New System.Drawing.Size(80, 30)
        Me.button10.TabIndex = 31
        Me.button10.Text = "最後一筆"
        Me.button10.UseVisualStyleBackColor = True
        '
        'FExit
        '
        Me.FExit.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.FExit.Location = New System.Drawing.Point(418, 5)
        Me.FExit.Name = "FExit"
        Me.FExit.Size = New System.Drawing.Size(80, 30)
        Me.FExit.TabIndex = 27
        Me.FExit.Text = "離開"
        Me.FExit.UseVisualStyleBackColor = True
        '
        'button9
        '
        Me.button9.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button9.Location = New System.Drawing.Point(256, 34)
        Me.button9.Name = "button9"
        Me.button9.Size = New System.Drawing.Size(80, 30)
        Me.button9.TabIndex = 30
        Me.button9.Text = "下一筆"
        Me.button9.UseVisualStyleBackColor = True
        '
        'button3
        '
        Me.button3.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button3.Location = New System.Drawing.Point(175, 5)
        Me.button3.Name = "button3"
        Me.button3.Size = New System.Drawing.Size(80, 30)
        Me.button3.TabIndex = 24
        Me.button3.Text = "刪除"
        Me.button3.UseVisualStyleBackColor = True
        '
        'button8
        '
        Me.button8.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button8.Location = New System.Drawing.Point(175, 34)
        Me.button8.Name = "button8"
        Me.button8.Size = New System.Drawing.Size(80, 30)
        Me.button8.TabIndex = 29
        Me.button8.Text = "上一筆"
        Me.button8.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button2.Location = New System.Drawing.Point(94, 5)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(80, 30)
        Me.button2.TabIndex = 23
        Me.button2.Text = "修改"
        Me.button2.UseVisualStyleBackColor = True
        '
        'button7
        '
        Me.button7.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button7.Location = New System.Drawing.Point(94, 34)
        Me.button7.Name = "button7"
        Me.button7.Size = New System.Drawing.Size(80, 30)
        Me.button7.TabIndex = 28
        Me.button7.Text = "第一筆"
        Me.button7.UseVisualStyleBackColor = True
        '
        'button4
        '
        Me.button4.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button4.Location = New System.Drawing.Point(256, 5)
        Me.button4.Name = "button4"
        Me.button4.Size = New System.Drawing.Size(80, 30)
        Me.button4.TabIndex = 25
        Me.button4.Text = "確定"
        Me.button4.UseVisualStyleBackColor = True
        '
        'button5
        '
        Me.button5.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.button5.Location = New System.Drawing.Point(337, 5)
        Me.button5.Name = "button5"
        Me.button5.Size = New System.Drawing.Size(80, 30)
        Me.button5.TabIndex = 26
        Me.button5.Text = "取消"
        Me.button5.UseVisualStyleBackColor = True
        '
        'buttonInsert
        '
        Me.buttonInsert.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.buttonInsert.Location = New System.Drawing.Point(13, 5)
        Me.buttonInsert.Name = "buttonInsert"
        Me.buttonInsert.Size = New System.Drawing.Size(80, 30)
        Me.buttonInsert.TabIndex = 22
        Me.buttonInsert.Text = "新增"
        Me.buttonInsert.UseVisualStyleBackColor = True
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.textBoxTEL4)
        Me.groupBox1.Controls.Add(Me.textBoxTEL2)
        Me.groupBox1.Controls.Add(Me.textBoxRName)
        Me.groupBox1.Controls.Add(Me.label12)
        Me.groupBox1.Controls.Add(Me.label11)
        Me.groupBox1.Controls.Add(Me.label10)
        Me.groupBox1.Controls.Add(Me.textBoxRemark)
        Me.groupBox1.Controls.Add(Me.textBoxOccupation)
        Me.groupBox1.Controls.Add(Me.textBoxTEL3)
        Me.groupBox1.Controls.Add(Me.textBoxTEL1)
        Me.groupBox1.Controls.Add(Me.textBoxCName)
        Me.groupBox1.Controls.Add(Me.label9)
        Me.groupBox1.Controls.Add(Me.label8)
        Me.groupBox1.Controls.Add(Me.label7)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.label5)
        Me.groupBox1.Font = New System.Drawing.Font("新細明體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.groupBox1.ForeColor = System.Drawing.Color.Red
        Me.groupBox1.Location = New System.Drawing.Point(185, 180)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Size = New System.Drawing.Size(606, 316)
        Me.groupBox1.TabIndex = 2
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "客戶資料"
        '
        'textBoxTEL4
        '
        Me.textBoxTEL4.Location = New System.Drawing.Point(81, 106)
        Me.textBoxTEL4.Name = "textBoxTEL4"
        Me.textBoxTEL4.Size = New System.Drawing.Size(518, 27)
        Me.textBoxTEL4.TabIndex = 31
        '
        'textBoxTEL2
        '
        Me.textBoxTEL2.Location = New System.Drawing.Point(381, 49)
        Me.textBoxTEL2.MaxLength = 10
        Me.textBoxTEL2.Name = "textBoxTEL2"
        Me.textBoxTEL2.Size = New System.Drawing.Size(218, 27)
        Me.textBoxTEL2.TabIndex = 30
        '
        'textBoxRName
        '
        Me.textBoxRName.Location = New System.Drawing.Point(381, 21)
        Me.textBoxRName.MaxLength = 5
        Me.textBoxRName.Name = "textBoxRName"
        Me.textBoxRName.Size = New System.Drawing.Size(218, 27)
        Me.textBoxRName.TabIndex = 29
        '
        'label12
        '
        Me.label12.AutoSize = True
        Me.label12.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label12.ForeColor = System.Drawing.Color.Black
        Me.label12.Location = New System.Drawing.Point(6, 109)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(64, 16)
        Me.label12.TabIndex = 28
        Me.label12.Text = "註 記 二"
        '
        'label11
        '
        Me.label11.AutoSize = True
        Me.label11.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label11.ForeColor = System.Drawing.Color.Black
        Me.label11.Location = New System.Drawing.Point(306, 49)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(64, 16)
        Me.label11.TabIndex = 27
        Me.label11.Text = "電 話 二"
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label10.ForeColor = System.Drawing.Color.Black
        Me.label10.Location = New System.Drawing.Point(306, 21)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(64, 16)
        Me.label10.TabIndex = 26
        Me.label10.Text = "介 紹 人"
        '
        'textBoxRemark
        '
        Me.textBoxRemark.Location = New System.Drawing.Point(81, 172)
        Me.textBoxRemark.Multiline = True
        Me.textBoxRemark.Name = "textBoxRemark"
        Me.textBoxRemark.Size = New System.Drawing.Size(518, 138)
        Me.textBoxRemark.TabIndex = 25
        '
        'textBoxOccupation
        '
        Me.textBoxOccupation.Location = New System.Drawing.Point(81, 142)
        Me.textBoxOccupation.MaxLength = 10
        Me.textBoxOccupation.Name = "textBoxOccupation"
        Me.textBoxOccupation.Size = New System.Drawing.Size(518, 27)
        Me.textBoxOccupation.TabIndex = 24
        '
        'textBoxTEL3
        '
        Me.textBoxTEL3.Location = New System.Drawing.Point(81, 77)
        Me.textBoxTEL3.Name = "textBoxTEL3"
        Me.textBoxTEL3.Size = New System.Drawing.Size(518, 27)
        Me.textBoxTEL3.TabIndex = 23
        '
        'textBoxTEL1
        '
        Me.textBoxTEL1.Location = New System.Drawing.Point(81, 49)
        Me.textBoxTEL1.MaxLength = 10
        Me.textBoxTEL1.Name = "textBoxTEL1"
        Me.textBoxTEL1.Size = New System.Drawing.Size(219, 27)
        Me.textBoxTEL1.TabIndex = 22
        '
        'textBoxCName
        '
        Me.textBoxCName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.textBoxCName.Location = New System.Drawing.Point(81, 21)
        Me.textBoxCName.MaxLength = 5
        Me.textBoxCName.Name = "textBoxCName"
        Me.textBoxCName.Size = New System.Drawing.Size(219, 27)
        Me.textBoxCName.TabIndex = 21
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label9.ForeColor = System.Drawing.Color.Black
        Me.label9.Location = New System.Drawing.Point(6, 175)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(72, 16)
        Me.label9.TabIndex = 20
        Me.label9.Text = "詳細內容"
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label8.ForeColor = System.Drawing.Color.Black
        Me.label8.Location = New System.Drawing.Point(6, 145)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(40, 16)
        Me.label8.TabIndex = 19
        Me.label8.Text = "備註"
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label7.ForeColor = System.Drawing.Color.Black
        Me.label7.Location = New System.Drawing.Point(6, 77)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(64, 16)
        Me.label7.TabIndex = 18
        Me.label7.Text = "註 記 一"
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label6.ForeColor = System.Drawing.Color.Black
        Me.label6.Location = New System.Drawing.Point(6, 52)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(64, 16)
        Me.label6.TabIndex = 17
        Me.label6.Text = "電 話 一"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label5.ForeColor = System.Drawing.Color.Black
        Me.label5.Location = New System.Drawing.Point(6, 24)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(56, 16)
        Me.label5.TabIndex = 16
        Me.label5.Text = "姓    名"
        '
        'dataGridView1
        '
        Me.dataGridView1.AllowUserToAddRows = False
        Me.dataGridView1.AllowUserToDeleteRows = False
        Me.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridView1.Location = New System.Drawing.Point(185, 3)
        Me.dataGridView1.Name = "dataGridView1"
        Me.dataGridView1.ReadOnly = True
        Me.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dataGridView1.RowTemplate.Height = 24
        Me.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataGridView1.Size = New System.Drawing.Size(606, 171)
        Me.dataGridView1.TabIndex = 1
        '
        'panel3
        '
        Me.panel3.BackColor = System.Drawing.SystemColors.Control
        Me.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel3.Controls.Add(Me.panel4)
        Me.panel3.Controls.Add(Me.label3)
        Me.panel3.Controls.Add(Me.label13)
        Me.panel3.Controls.Add(Me.label4)
        Me.panel3.Location = New System.Drawing.Point(3, 3)
        Me.panel3.Name = "panel3"
        Me.panel3.Size = New System.Drawing.Size(176, 563)
        Me.panel3.TabIndex = 0
        '
        'panel4
        '
        Me.panel4.BackColor = System.Drawing.SystemColors.ControlLight
        Me.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel4.Controls.Add(Me.textBoxSearch)
        Me.panel4.Controls.Add(Me.button1)
        Me.panel4.Controls.Add(Me.label2)
        Me.panel4.Controls.Add(Me.label1)
        Me.panel4.Location = New System.Drawing.Point(-1, -4)
        Me.panel4.Name = "panel4"
        Me.panel4.Size = New System.Drawing.Size(176, 471)
        Me.panel4.TabIndex = 0
        '
        'textBoxSearch
        '
        Me.textBoxSearch.Location = New System.Drawing.Point(6, 50)
        Me.textBoxSearch.Name = "textBoxSearch"
        Me.textBoxSearch.Size = New System.Drawing.Size(165, 22)
        Me.textBoxSearch.TabIndex = 7
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(96, 304)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(75, 23)
        Me.button1.TabIndex = 9
        Me.button1.Text = "執行"
        Me.button1.UseVisualStyleBackColor = True
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label2.Location = New System.Drawing.Point(3, 31)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(40, 16)
        Me.label2.TabIndex = 8
        Me.label2.Text = "電話"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("新細明體", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.label1.Location = New System.Drawing.Point(64, 3)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(54, 21)
        Me.label1.TabIndex = 1
        Me.label1.Text = "查詢"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label3.Location = New System.Drawing.Point(3, 483)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(56, 16)
        Me.label3.TabIndex = 10
        Me.label3.Text = "筆數："
        '
        'label13
        '
        Me.label13.AutoSize = True
        Me.label13.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label13.Location = New System.Drawing.Point(117, 483)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(24, 16)
        Me.label13.TabIndex = 12
        Me.label13.Text = "筆"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.label4.Location = New System.Drawing.Point(65, 483)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(16, 16)
        Me.label4.TabIndex = 11
        Me.label4.Text = "0"
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel5.Controls.Add(Me.panel2)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 132)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(957, 609)
        Me.Panel5.TabIndex = 10
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(957, 741)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.menuStrip1)
        Me.Controls.Add(Me.labelState)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "客戶管理子系統CUSMenu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.menuStrip1.ResumeLayout(False)
        Me.menuStrip1.PerformLayout()
        Me.panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        CType(Me.dataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel3.ResumeLayout(False)
        Me.panel3.PerformLayout()
        Me.panel4.ResumeLayout(False)
        Me.panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents menuStrip1 As System.Windows.Forms.MenuStrip
    Private WithEvents 結束作業系統ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 客戶基本資料ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents 密碼變更ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents labelState As System.Windows.Forms.Label
    Private WithEvents panel2 As System.Windows.Forms.Panel
    Private WithEvents button10 As System.Windows.Forms.Button
    Private WithEvents button9 As System.Windows.Forms.Button
    Private WithEvents button8 As System.Windows.Forms.Button
    Private WithEvents button7 As System.Windows.Forms.Button
    Private WithEvents button5 As System.Windows.Forms.Button
    Private WithEvents FExit As System.Windows.Forms.Button
    Private WithEvents buttonInsert As System.Windows.Forms.Button
    Private WithEvents button4 As System.Windows.Forms.Button
    Private WithEvents button2 As System.Windows.Forms.Button
    Private WithEvents button3 As System.Windows.Forms.Button
    Private WithEvents groupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents textBoxTEL4 As System.Windows.Forms.TextBox
    Private WithEvents textBoxTEL2 As System.Windows.Forms.TextBox
    Private WithEvents textBoxRName As System.Windows.Forms.TextBox
    Private WithEvents label12 As System.Windows.Forms.Label
    Private WithEvents label11 As System.Windows.Forms.Label
    Private WithEvents label10 As System.Windows.Forms.Label
    Private WithEvents textBoxRemark As System.Windows.Forms.TextBox
    Private WithEvents textBoxOccupation As System.Windows.Forms.TextBox
    Private WithEvents textBoxTEL3 As System.Windows.Forms.TextBox
    Private WithEvents textBoxTEL1 As System.Windows.Forms.TextBox
    Private WithEvents textBoxCName As System.Windows.Forms.TextBox
    Private WithEvents label9 As System.Windows.Forms.Label
    Private WithEvents label8 As System.Windows.Forms.Label
    Private WithEvents label7 As System.Windows.Forms.Label
    Private WithEvents label6 As System.Windows.Forms.Label
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents dataGridView1 As System.Windows.Forms.DataGridView
    Private WithEvents panel3 As System.Windows.Forms.Panel
    Private WithEvents panel4 As System.Windows.Forms.Panel
    Private WithEvents textBoxSearch As System.Windows.Forms.TextBox
    Private WithEvents button1 As System.Windows.Forms.Button
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label13 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel

End Class
